# Nekomata

## Requirements

- Python 3.9
- Node.js
- Nodemon
- Pipenv

## Install Virtual Environment

```
pipenv install
```

## Run Bot

```
pipenv run dev
```
